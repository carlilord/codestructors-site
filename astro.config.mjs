import tailwind from "@astrojs/tailwind";
import { defineConfig } from "astro/config";

// https://astro.build/config
export default defineConfig({
  sitemap: true,
  site: "https://www.codestructors.at/",
  outDir: 'public',
  publicDir: 'static',
  integrations: [tailwind()],
  vite: {
    ssr: {
      external: ["svgo"],
    },
  },
});
