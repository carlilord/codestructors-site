---
layout: ../layouts/BaseLayout.astro
title: Impressum
---
Informationspflicht laut §5 E-Commerce Gesetz, §14 Unternehmensgesetzbuch, §63 Gewerbeordnung und Offenlegungspflicht laut §25 Mediengesetz.

codestructors e.U.
Carl Weilguny
Rosenstraße 15,
4040 Linz,
Österreich

Unternehmensgegenstand: IT Dienstleistungen
Firmenbuchnummer: FN 593545k
Firmenbuchgericht: Landesgericht Linz
Firmensitz: 4040 Linz

E-Mail: office[at]codestructors.at

Mitglied bei: WKO, Oberösterreich.
Berufsrecht: Gewerbeordnung: www.ris.bka.gv.at

Aufsichtsbehörde/Gewerbebehörde: Bezirkshauptmannschaft Linz
Berufsbezeichnung: Programmierer
Verleihungsstaat: Österreich

Kontaktdaten des Verantwortlichen für Datenschutz
Sollten Sie Fragen zum Datenschutz haben, finden Sie nachfolgend die Kontaktdaten der verantwortlichen Person bzw. Stelle:
codestructors e.U.
Carl Weilguny
Rosenstraße 15,
4040 Linz,
Österreich
Vertretungsberechtigt: Carl Weilguny
E-Mail-Adresse: office[at]codestructors.at
Impressum: https://www.codestructors.at/impressum/


### EU-Streitschlichtung

Gemäß Verordnung über Online-Streitbeilegung in Verbraucherangelegenheiten (ODR-Verordnung) möchten wir Sie über die Online-Streitbeilegungsplattform (OS-Plattform) informieren.
Verbraucher haben die Möglichkeit, Beschwerden an die Online Streitbeilegungsplattform der Europäischen Kommission unter http://ec.europa.eu/odr?tid=121890417 zu richten. Die dafür notwendigen Kontaktdaten finden Sie oberhalb in unserem Impressum.

Wir möchten Sie jedoch darauf hinweisen, dass wir nicht bereit oder verpflichtet sind, an Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

### Haftung für Inhalte dieser Website

Wir entwickeln die Inhalte dieser Website ständig weiter und bemühen uns korrekte und aktuelle Informationen bereitzustellen.  Leider können wir keine Haftung für die Korrektheit aller Inhalte auf dieser Website übernehmen, speziell für jene, die seitens Dritter bereitgestellt wurden. Als Diensteanbieter sind wir nicht verpflichtet, die von ihnen übermittelten oder gespeicherten Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen.

Unsere Verpflichtungen zur Entfernung von Informationen oder zur Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen aufgrund von gerichtlichen oder behördlichen Anordnungen bleiben auch im Falle unserer Nichtverantwortlichkeit davon unberührt.

Sollten Ihnen problematische oder rechtswidrige Inhalte auffallen, bitte wir Sie uns umgehend zu kontaktieren, damit wir die rechtswidrigen Inhalte entfernen können. Sie finden die Kontaktdaten im Impressum.

### Haftung für Links auf dieser Website

Unsere Website enthält Links zu anderen Websites für deren Inhalt wir nicht verantwortlich sind. Haftung für verlinkte Websites besteht für uns nicht, da wir keine Kenntnis rechtswidriger Tätigkeiten hatten und haben, uns solche Rechtswidrigkeiten auch bisher nicht aufgefallen sind und wir Links sofort entfernen würden, wenn uns Rechtswidrigkeiten bekannt werden.

Wenn Ihnen rechtswidrige Links auf unserer Website auffallen, bitte wir Sie uns zu kontaktieren. Sie finden die Kontaktdaten im Impressum.

### Urheberrechtshinweis

Alle Inhalte dieser Webseite (Bilder, Fotos, Texte, Videos) unterliegen dem Urheberrecht. Bitte fragen Sie uns bevor Sie die Inhalte dieser Website verbreiten, vervielfältigen oder verwerten wie zum Beispiel auf anderen Websites erneut veröffentlichen. Falls notwendig, werden wir die unerlaubte Nutzung von Teilen der Inhalte unserer Seite rechtlich verfolgen.

Sollten Sie auf dieser Webseite Inhalte finden, die das Urheberrecht verletzen, bitten wir Sie uns zu kontaktieren.

### Bildernachweis

Die Bilder, Fotos und Grafiken auf dieser Webseite sind von Pixabay.

Alle Texte sind urheberrechtlich geschützt.

Quelle: Erstellt mit dem <a title="Impressum Generator von AdSimple für Österreich" href="https://www.adsimple.at/impressum-generator/">Impressum Generator</a> von AdSimple
